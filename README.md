https://github.com/docker-library/docs/blob/master/postgres/README.md
https://github.com/felipewom/docker-compose-postgres
https://www.pgadmin.org/docs/pgadmin4/latest/container_deployment.html

```bash

sudo docker run -i --rm postgres:16-alpine cat /usr/local/share/postgresql/postgresql.conf.sample > my-postgres.conf

sudo docker network create pg;

sudo docker volume create var-lib-postgresql-data;
sudo docker volume create var-lib-pgadmin;

sudo mkdir bind;
sudo mkdir -p bind/etc-pgdump;

# sudo mkdir bind/var-lib-pgadmin
# sudo chown -R 5050:5050 ./bind/var-lib-pgadmin

sudo docker compose --profile main up -d
sudo docker compose --profile admin up -d

# dkx into 'main' container -> PostgreSQL interactive terminal
psql -d $POSTGRES_DB -U $POSTGRES_USER -p $POSTGRES_INNER_PORT
# make some queries
SELECT * FROM public.playground;
# drop all tables in schema `public`
DROP SCHEMA public CASCADE;
CREATE SCHEMA public;
# \q -> exit

```
