#!/bin/sh

sudo docker network create pg;
sudo docker volume create var-lib-postgresql-data;
sudo docker volume create var-lib-pgadmin;
mkdir bind;
mkdir -p bind/etc-pgdump;