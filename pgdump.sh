#!/bin/bash

datetime="$(date +"%Y-%m-%d-%H%M")";

host=$PG_DUMP_HOST;
port=$PG_DUMP_PORT;
username=$PG_DUMP_USER;
password=$PG_DUMP_PASSWORD;
dbname=$PG_DUMP_DB;

netloc=""

if [[ $host =~ .*:.* ]]
then
    netloc="[${host}]:${port}" # IPv6
else
    netloc="${host}:${port}" # IPv4
fi

connStrings="postgresql://${username}:${password}@${netloc}/${dbname}";

parentDir="/etc/pgdump";
instanceName=$PG_DUMP_INSTANCE_NAME;
tableNameOrPattern=$PG_DUMP_TABLE_NAME_OR_PATTERN;
tableNameOrPatternExclude=$PG_DUMP_TABLE_NAME_OR_PATTERN_EXCLUDE;

format=$PG_DUMP_FORMAT;

printf $datetime;

if [[ -z $tableNameOrPattern && -z $tableNameOrPatternExclude ]]; then
    pg_dump --format=$format --verbose --encoding "UTF8" --no-owner --no-privileges \
            -d $connStrings --file "${parentDir}/pgdump-${instanceName}-${dbname}-full-${datetime}";
elif [[ -n $tableNameOrPattern && -n $tableNameOrPatternExclude ]]; then
    pg_dump --format=$format --verbose --encoding "UTF8" --no-owner --no-privileges \
            --table $tableNameOrPattern --exclude-table $tableNameOrPatternExclude \
            -d $connStrings --file "${parentDir}/pgdump-${instanceName}-${dbname}-partial-${datetime}";
elif [[ -n $tableNameOrPattern ]]; then
    pg_dump --format=$format --verbose --encoding "UTF8" --no-owner --no-privileges \
            --table $tableNameOrPattern \
            -d $connStrings --file "${parentDir}/pgdump-${instanceName}-${dbname}-partial-${datetime}";
else
    pg_dump --format=$format --verbose --encoding "UTF8" --no-owner --no-privileges \
            --exclude-table $tableNameOrPatternExclude \
            -d $connStrings --file "${parentDir}/pgdump-${instanceName}-${dbname}-partial-${datetime}";
fi