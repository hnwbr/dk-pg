#!/bin/sh

host="localhost";
port=$POSTGRES_OUTER_PORT;
username=$POSTGRES_USER;
password=$POSTGRES_PASSWORD;
dbname=$POSTGRES_DB;

connStrings="postgresql://${username}:${password}@${host}:${port}/${dbname}";

parentDir="/etc/pgdump";
filePattern=$PG_RESTORE_FILE_PATTERN;
filePath="$(find $parentDir -type f -name "*${filePattern}*" | sort | tail -n 1)";

printf "$filePath";

pg_restore --verbose --no-owner --no-acl -d $connStrings $filePath;